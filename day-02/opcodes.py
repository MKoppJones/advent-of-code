content = []
with open('input.txt') as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

def run_program(program):
    steps = program.split(',')
    pointer = 0
    while (steps[pointer] != '99'):
        op = int(steps[pointer])
        vali1 = int(steps[pointer + 1])
        vali2 = int(steps[pointer + 2])
        val1 = int(steps[vali1])
        val2 = int(steps[vali2])
        res = int(steps[pointer + 3])
        answer = 0
        if (op == 1):
            answer = val1 + val2
        elif (op == 2):
            answer = val1 * val2
        steps[res] = str(answer)
        pointer += 4
    return ','.join(steps)

def fix_computer():
    res = 0
    for program in content:
        p = program.split(',')
        p[1] = '12'
        p[2] = '2'
        pro = ','.join(p)
        res = run_program(pro)
    return res.split(',')[0]

if __name__ == '__main__':
    print(fix_computer())