from opcodes import IntCodeInterpreter, IntCodeProgram

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    program_string = content[0]
    interpreter = IntCodeInterpreter()
    program = IntCodeProgram(program_string)
    interpreter.init(program, True, 1)
    
    scaffolds = {}
    line = ''
    x = 0
    y = 0
    while not interpreter.halted:
        data = interpreter.exec([])
        if len(data) > 0:
            if data[0] == 10:
                print(line)
                line = ''
                x = 0
                y += 1
            else:
                scaffolds[(x, y)] = str(chr(data[0]))
                line += str(chr(data[0]))
                x += 1

    y = 0
    
    value = 0
    
    for key in scaffolds.keys():
        pointer = key
        if scaffolds[key] == '#':
            top = (key[0], key[1] - 1)
            bottom = (key[0], key[1] + 1)
            left = (key[0] - 1, key[1])
            right = (key[0] + 1, key[1])
            
            if top in scaffolds and bottom in scaffolds and left in scaffolds and right in scaffolds:
                intersection = scaffolds[top]
                intersection += scaffolds[bottom]
                intersection += scaffolds[left]
                intersection += scaffolds[right]
                if intersection == '####':
                    value += key[0] * key[1]
                    scaffolds[key] = 'O'
        
        if key[1] > y:
            
            print(line)
            line = ''
            y += 1
            
        line += scaffolds[key]

    print(value)