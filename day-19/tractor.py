from opcodes import IntCodeInterpreter, IntCodeProgram

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    program_string = content[0]
    interpreter = IntCodeInterpreter()
    interpreter.init(IntCodeProgram(program_string), True, 1)
    
    max_x = 50
    max_y = 50
    
    hit = 0
    
    for x in range(max_x):
        for y in range(max_y):
            interpreter.init(IntCodeProgram(program_string), True, 1)
            data = interpreter.exec([x,y])
            if data.pop() == 1:
                hit += 1
                
    print(hit)