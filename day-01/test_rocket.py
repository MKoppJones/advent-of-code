import pytest
from rocket import *

@pytest.mark.parametrize("mass, res", [(12, 2), (14, 2), (1969, 654), (100756, 33583)])
def test_fuel(mass, res):
    assert calculate_fuel_required(mass) == res