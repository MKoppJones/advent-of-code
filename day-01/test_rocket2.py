import pytest
from rocket2 import *

@pytest.mark.parametrize("mass, res", [(14, 2), (1969, 966), (100756, 50346)])
def test_fuel2(mass, res):
    assert calculate_fuel_required(mass) == res