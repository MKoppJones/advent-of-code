from anytree import Node, RenderTree
    
# def counter(planets, current_planet):
#     count = len(current_planet.orbiters)
    
#     for orbiter in current_planet.orbiters:
#         count += counter(planets, orbiter)

#     return count

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    root = Node('COM')
    