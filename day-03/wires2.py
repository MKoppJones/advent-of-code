import sys
import collections

def count_steps(wire1, wire2):        
    steps_taken = {}
    x, y = 0, 0
    steps = 1
    wire1_steps = wire1.split(',')
    wire2_steps = wire2.split(',')
    
    for step in wire1_steps:
        d = step[0]
        dist = int(step[1:])
        
        for _ in range(dist):
            if (d == 'R'):
                x += 1
            elif (d == 'L'):
                x -= 1
            elif (d == 'U'):
                y += 1
            elif (d == 'D'): 
                y -= 1
                
            if (x, y) not in steps_taken:
                steps_taken[(x, y)] = steps
            steps += 1

    collisions = {}
    x, y = 0, 0
    steps = 1
    
    for step in wire2_steps:
        d = step[0]
        dist = int(step[1:])
        
        for _ in range(dist):
            if (d == 'R'):
                x += 1
            elif (d == 'L'):
                x -= 1
            elif (d == 'U'):
                y += 1
            elif (d == 'D'): 
                y -= 1
                
            if (x, y) in steps_taken:
                collisions[(x, y)] = steps + steps_taken[(x, y)]
            steps += 1
            
    return min(collisions.values())

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    print(count_steps(content[0], content[1]))