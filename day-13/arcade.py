from opcodes import IntCodeInterpreter, IntCodeProgram

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    program_string = content[0]
    interpreter = IntCodeInterpreter()
    program = IntCodeProgram(program_string)
    interpreter.init(program, True, 3)
    
    screen = {}
    while not interpreter.halted:
        data = interpreter.exec([])
        if len(data) > 0:
            if (data[0], data[1]) not in screen:
                screen[(data[0], data[1])] = data[2]
            else:
                screen[(data[0], data[1])] = data[2]
    
    count = 0
    for pixel, value in screen.items():
        count += 1 if value == 2 else 0
    print(count)