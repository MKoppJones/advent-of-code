from opcodes import IntCodeInterpreter, IntCodeProgram
from collections import defaultdict

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    program_string = content[0]
    interpreter = IntCodeInterpreter()
    program = IntCodeProgram(program_string)
    interpreter.init(program, True, 2)
    
    robot_location = (0,0)
    facing = 1  # < ^ > v = 0 1 2 3
    hull = defaultdict(lambda: 0)
    painted = []
    
    while not interpreter.halted:
        start = [hull[robot_location]]
        output = interpreter.exec(start)
        
        if len(output) == 0:
            break
        
        hull[robot_location] = output[0]
        
        # This location has been visited, so store it
        if robot_location not in painted:
            painted.append(robot_location)
            
        if len(output) > 1:
            if output[1] == 0:
                facing -= 1
                if facing < 0:
                    facing = 3
            else:
                facing += 1
                if facing > 3:
                    facing = 0
                    
        if facing == 0:
            robot_location = (robot_location[0] - 1, robot_location[1])
        elif facing == 1:
            robot_location = (robot_location[0], robot_location[1] + 1)
        elif facing == 2:
            robot_location = (robot_location[0] + 1, robot_location[1])
        elif facing == 3:
            robot_location = (robot_location[0], robot_location[1] - 1)
        
    print(len(painted))
    
    