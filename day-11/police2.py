from opcodes import IntCodeInterpreter, IntCodeProgram
from collections import defaultdict

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    
    program_string = content[0]
    interpreter = IntCodeInterpreter()
    program = IntCodeProgram(program_string)
    interpreter.init(program, True, 2)
    
    robot_location = (0,30)
    facing = 1  # < ^ > v = 0 1 2 3
    hull = defaultdict(lambda: 0)
    hull[robot_location] = 1
    painted = []
    
    while not interpreter.halted:
        start = [hull[robot_location]]
        output = interpreter.exec(start)
        
        if len(output) == 0:
            break
        
        hull[robot_location] = output[0]
        
        # This location has been visited, so store it
        if robot_location not in painted:
            painted.append(robot_location)
            
        if len(output) > 1:
            if output[1] == 0:
                facing -= 1
                if facing < 0:
                    facing = 3
            else:
                facing += 1
                if facing > 3:
                    facing = 0
                    
        if facing == 0:
            robot_location = (robot_location[0] - 1, robot_location[1])
        elif facing == 1:
            robot_location = (robot_location[0], robot_location[1] + 1)
        elif facing == 2:
            robot_location = (robot_location[0] + 1, robot_location[1])
        elif facing == 3:
            robot_location = (robot_location[0], robot_location[1] - 1)
    
    rows = []
    maxy = 0
    for pixel in hull:
        if pixel[1] > maxy:
            maxy = pixel[1]
    for pixel, value in hull.items():
        while maxy - pixel[1] + 1 > len(rows):
            rows.append([])
            
        while len(rows[maxy - pixel[1]]) < pixel[0] + 1:
            rows[maxy - pixel[1]].append(' ')
        
        rows[maxy - pixel[1]][pixel[0]] = ' ' if value == 0 else u'\u2588'
     
    for row in rows:
        print(''.join(row))