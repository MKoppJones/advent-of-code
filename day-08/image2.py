if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content][0]
    
    index = 0
    size = 25 * 6
    
    master = []
    
    while index < len(content):
        layer = content[index:index+size]
        if len(master) == 0:
            master = list(layer)
        else:
            for pixel_index in range(0, len(layer)):
                master_pixel = int(master[pixel_index])
                layer_pixel = int(layer[pixel_index])
                if master_pixel == 2:
                    master[pixel_index] = layer[pixel_index]
        index += size
    width = 25
    index = 0
    while index < len(master):
        line = master[index:index+width]
        print_line = ''
        for x in line:
            if int(x) == 0:
                print_line += ' '
            else: 
                print_line += u'\u2588'
        print(print_line)
        index += width
    