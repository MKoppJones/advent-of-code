from opcodes import *

def unique_values(settings):
    dic = {}
    for x in settings:
        if x not in dic:
            dic[x] = 1
        else:
            return False
    return True

if __name__ == '__main__':
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    program_string = content[0]
    interpreter = IntCodeInterpreter()
    
    thruster = 0
    best_settings = []
    
    amp_a = IntCodeInterpreter()
    amp_b = IntCodeInterpreter()
    amp_c = IntCodeInterpreter()
    amp_d = IntCodeInterpreter()
    amp_e = IntCodeInterpreter()
    for a in range(5, 10):
        for b in range(5, 10):
            for c in range(5, 10):
                for d in range(5, 10):
                    for e in range(5, 10):
                        if unique_values([a,b,c,d,e]):
    
                            amp_a.init(IntCodeProgram(program_string), True)
                            amp_b.init(IntCodeProgram(program_string), True)
                            amp_c.init(IntCodeProgram(program_string), True)
                            amp_d.init(IntCodeProgram(program_string), True)
                            amp_e.init(IntCodeProgram(program_string), True)
                            ao = 0
                            bo = 0
                            co = 0
                            do = 0
                            eo = 0
                            
                            a_input = [a, 0]
                            b_input = [b]
                            c_input = [c]
                            d_input = [d]
                            e_input = [e]
                            count = 0
                            while not amp_e.halted:
                                count += 1
                                print(count)
                                ao = amp_a.exec(a_input)
                                b_input.append(ao)
                                bo = amp_b.exec(b_input)
                                c_input.append(bo)
                                co = amp_c.exec(c_input)
                                d_input.append(co)
                                do = amp_d.exec(d_input)
                                e_input.append(do)
                                eo = amp_e.exec(e_input)
                                a_input.append(eo)
                            if int(eo) > thruster:
                                thruster = int(eo)
                                best_settings = [a,b,c,d,e]
    print (thruster, best_settings)
    