import re

def count_passwords(r):
    extremes = r.split('-')
    start = int(extremes[0])
    end = int (extremes[1])
    count = 0
    for password in range(start, end):
        if password_valid(str(password)):
            count += 1
    return count

def password_valid(password):
    digits = re.search(r"(\d)\1+", password)
    if digits is None:
        return False
    
    prev = 0
    for d in password:
        digit = int(d)
        if prev > digit:
            return False
        prev = digit
    
    return True

if __name__ == '__main__':
    print(count_passwords('372304-847060'))