import pytest
from container2 import *

@pytest.mark.parametrize("password, res", [('112233', True), ('333444', False), ('111122', True), ('124444', False)])
def test_password(password, res):
    assert password_valid(password) == res