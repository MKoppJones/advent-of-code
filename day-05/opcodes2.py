content = []
with open('input.txt') as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

def run_program(program):
    steps = program.split(',')
    pointer = 0
    while (steps[pointer] != '99'):
        op = int(steps[pointer])
        vali1 = int(steps[pointer + 1])
        vali2 = int(steps[pointer + 2])
        val1 = int(steps[vali1])
        val2 = int(steps[vali2])
        res = int(steps[pointer + 3])
        answer = 0
        if (op == 1):
            answer = val1 + val2
        elif (op == 2):
            answer = val1 * val2
        steps[res] = str(answer)
        pointer += 4
    return ','.join(steps)

def fix_computer():
    res = 0
    program = content[0]
    int1 = 0
    int2 = 0
    for noun in range(0, 99):
        for verb in range(0, 99):
            p = program.split(',')
            p[1] = str(noun)
            p[2] = str(verb)
            pro = ','.join(p)
            res = run_program(pro)
            if (int(res.split(',')[0]) == 19690720):
                int2 = verb
                int1 = noun
    return 100 * int1 + int2

if __name__ == '__main__':
    print(fix_computer())