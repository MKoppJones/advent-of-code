class IntCodeProgram:
    instructions = []
    def __init__(self, program):
        str_instructions = program.split(',')
        self.instructions = [int(num) for num in str_instructions]

class IntCodeInterpreter:
    program = {}
    instruction_pointer = 0
    input_value = 0
    output_value = 0
    halt_at = 99
    operation_modes = []
    parameters = []
    def __init__(self):
        self.program = {}
        self.instruction_pointer = 0
        self.input_value = 0
    
    def exec(self, program, input_value):
        self.input_value = input_value
        self.program = program
        while self.program.instructions[self.instruction_pointer] != self.halt_at:
            current_instruction = str(program.instructions[self.instruction_pointer]).zfill(5)
            operation = int(current_instruction[-2:])
            print(current_instruction, current_instruction[2], current_instruction[1], current_instruction[0])
            self.operation_modes = [current_instruction[2], current_instruction[1], current_instruction[0]]
            self.run_operation(operation)
        return self.output_value

    def run_operation(self, operation):
        if operation == 1:
            self.add()
        elif operation == 2:
            self.multiply()
        elif operation == 3:
            self.save()
        elif operation == 4:
            self.read()
        elif operation == 5:
            self.jump_if_true()
        elif operation == 6:
            self.jump_if_false()
        elif operation == 7:
            self.less_than()
        elif operation == 8:
            self.equals()
    
    def parse_parameters(self, modes):
        values = []
        index = 1
        for mode in modes:
            location = self.program.instructions[self.instruction_pointer + index]
            if mode == '-':
                values.append(0)
            elif int(mode) == 1:
                values.append(location)
            else:
                if location < len(self.program.instructions) and location > 0:
                    values.append(self.program.instructions[location])
                else:
                    values.append(0)
            index += 1
        return values
    
    def parse_parameter(self, pointer, index):
        mode = int(self.operation_modes[index])
        location = int(self.program.instructions[pointer])
        if mode == 1:
            return location
        return int(self.program.instructions[location])

    def add(self):
        output_location = self.program.instructions[self.instruction_pointer + 3]
        self.program.instructions[output_location] = self.parse_parameter(self.instruction_pointer + 1, 0) + self.parse_parameter(self.instruction_pointer + 2, 1)
        self.instruction_pointer += 4
     
    def multiply(self):
        output_location = self.program.instructions[self.instruction_pointer + 3]
        self.program.instructions[output_location] = self.parse_parameter(self.instruction_pointer + 1, 0) * self.parse_parameter(self.instruction_pointer + 2, 1)
        self.instruction_pointer += 4
    
    def save(self):
        output_location = self.program.instructions[self.instruction_pointer + 1]
        self.program.instructions[output_location] = self.input_value
        self.instruction_pointer += 2
    
    def read(self):
        output_location = self.program.instructions[self.instruction_pointer + 1]
        self.output_value = self.program.instructions[output_location]
        self.instruction_pointer += 2
    
    def jump_if_true(self):
        if self.parse_parameter(self.instruction_pointer + 1, 0) is not 0:
            self.instruction_pointer = self.parse_parameter(self.instruction_pointer + 2, 1)
        else:
            self.instruction_pointer += 3
    
    def jump_if_false(self):
        if self.parse_parameter(self.instruction_pointer + 1, 0) is 0:
            self.instruction_pointer = self.parse_parameter(self.instruction_pointer + 2, 1)
        else:
            self.instruction_pointer += 3
    
    def less_than(self):
        output_location = self.program.instructions[self.instruction_pointer + 3]
        if self.parse_parameter(self.instruction_pointer + 1, 0) < self.parse_parameter(self.instruction_pointer + 2, 1):
            self.program.instructions[output_location] = 1
        else:
            self.program.instructions[output_location] = 0
        self.instruction_pointer += 4
    
    def equals(self):
        output_location = self.program.instructions[self.instruction_pointer + 3]
        if self.parse_parameter(self.instruction_pointer + 1, 0) == self.parse_parameter(self.instruction_pointer + 2, 1):
            self.program.instructions[output_location] = 1
        else:
            self.program.instructions[output_location] = 0
        self.instruction_pointer += 4

def run_diag(input_value):
    content = []
    with open('input.txt') as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    program_string = content[0]
    int_code_program = IntCodeProgram(program_string)
    interpreter = IntCodeInterpreter()
    
    return_value = interpreter.exec(int_code_program, input_value)
    print(return_value)

if __name__ == '__main__':
    run_diag(5)